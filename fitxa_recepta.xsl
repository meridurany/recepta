<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/"> <!--comencem a treballar per l'arrel, receptes-->
  <html>
    <head>
    	<meta charset="UTF-8" />
      <title>Delicias Kitchen</title>
    
    <link rel="stylesheet" href="recetacss.css" />
 
    </head>
    <body>
       <header>
        <img alt="Delicias Kitchen" src="delicias.jpg" />
       </header>
       
        <nav id="fijo">
            <ul class="menunav">
                <li> <a href="#">Inicio</a></li>
                <li> <a href="#">Sobre mí</a></li>
                <li> <a href="#">Recetas</a></li>
                <li> <a href="#">Contacto</a></li>
                <li> <a href="#">Otras cosas</a></li>
            </ul>
        </nav>
 
   
    <article>
      <xsl:for-each select="receptes/recepta"> <!--amb al for-each fem com un bucle, com tenim més d'una recepta, demanem que es repeteixi per tantes receptes com tinguem, si només en tenim una, en farà una-->
        <div class="categoria">Recetas de <xsl:value-of select="categoria"/></div>
        <!--hem d'agafar el valor que hi ha dins de l'etiqueta categoria, hem de substituir pescado pel valor de l'etiqueta categoria-->
        <div class="titol"> <xsl:value-of select="nom"/></div>
        <div id="info">
          <xsl:element name="img">
              <xsl:attribute name="class">foto</xsl:attribute>
              <xsl:attribute name="src">
                <xsl:value-of select="informacio_general/foto"/>    
              </xsl:attribute>
          </xsl:element> <!-- FINS AQUI la imatge -->
          
          <div class="info2">
            <div class="general">
              <b>Començals:</b><xsl:value-of select="informacio_general/comensals"/> <br/> 
              <b>Temps:</b><xsl:value-of select="informacio_general/temps"/> <xsl:value-of select="informacio_general/temps/@unitat"/><br/> 
              <!--<b>Temps</b> <xsl:value-of select="informacio_general/temps" /> <xsl:value-of select="concat(' ',informacio_general/temps/@unitat)" /><br/>-->
              <b>Dificultat:</b><xsl:value-of select="informacio_general/dificultat"/> 
            </div>
          <!--ara posem els ingredients, fixa't que n'hi ha varis i no sabem quants-->
            <div id="ingredients">
              <span class="subtitol">Ingredients:</span><br/>
              <span class="llista_ingredients">
                <xsl:for-each select="ingredients/ingredient">
                  <xsl:value-of select="@quantitat"/><xsl:value-of select="concat(' ',@unitat,' ',.)"/> <br/><!--espai, atribut quantitat, espai, valor node-->
                </xsl:for-each>
              </span>
            </div> <!--ingredients-->
          </div><!--info2-->
        </div><!--info-->
        <div class="prepa">
          <span class="subtitol2">Preparació:</span> <br/>
            <!--llista de passos-->
            <xsl:for-each select="preparacio/pas">
            <span class="numero"> 
            	<xsl:value-of select="@numero"/></span>
            	<span class="pas"><xsl:value-of select="." /></span><br/>
          </xsl:for-each>
           <br/> 
         </div>
        </xsl:for-each> 
      </article>
    </body>
   </html>
 </xsl:template>
</xsl:stylesheet>
